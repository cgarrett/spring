import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sample.Application;
import org.sample.BeanA;
import org.sample.BeanB;
import org.sample.BeanC;
import org.sample.BeanUtils;
import org.sample.LogConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ch.qos.logback.classic.Level;


public class BasicContainerTests {

	private ApplicationContext context = null;
	private static Logger log = LoggerFactory.getLogger(BasicContainerTests.class);
	
	@Before
	public void setUp() {
		LogConfig.setLevelInfo();
		context = new ClassPathXmlApplicationContext(new String[] {"simple-scope.xml"});
		assertNotNull(context);
	}
	
	@After
	public void tearDown() {
		((ClassPathXmlApplicationContext) context).close();		
	}

	@Test
	public void testBeans() {
		
		assertTrue(BeanUtils.hasInstanceOfClass(context, BeanA.class));
		assertFalse(BeanUtils.hasInstanceOfClass(context, BeanB.class));
		assertFalse(BeanUtils.hasInstanceOfClass(context, BeanC.class));

		Object lazy = context.getBean(BeanC.class);
		assertNotNull(lazy);

		assertTrue(BeanUtils.hasInstanceOfClass(context, BeanA.class));
		assertTrue(BeanUtils.hasInstanceOfClass(context, BeanB.class));	
	}

}
