


import org.sample.LogConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ch.qos.logback.classic.Level;

public class MyContainer {
	
	private static Logger log = LoggerFactory.getLogger(MyContainer.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		LogConfig.setLevelInfo();
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"container.xml"});
		// will call destructors
		log.info("Closing container");
		((ClassPathXmlApplicationContext) context).close();
		log.info("Going out of scope");
	}

}
