package org.sample;

import org.sample.plugin.ApplicationPluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeanB {

	private static Logger log = LoggerFactory.getLogger(BeanB.class);
	
	BeanB() {
		log.info("ctor()");
	}
}
