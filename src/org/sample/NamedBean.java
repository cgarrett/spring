package org.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NamedBean {
	private static Logger log = LoggerFactory.getLogger(NamedBean.class);
	private String name = null;
	
	public NamedBean(String name) {
		log.info("ctor(): " + name);
		this.name = name;
	}
}
