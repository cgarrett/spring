package org.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeanA {
	private static Logger log = LoggerFactory.getLogger(BeanA.class);
	
	BeanA() {
		log.info("ctor()");
	}
}
