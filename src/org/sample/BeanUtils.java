package org.sample;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class BeanUtils {

	public static List<Object> getInstantiatedSigletons(ApplicationContext ctx) {
		List<Object> singletons = new ArrayList<Object>();

		String[] all = ctx.getBeanDefinitionNames();

		ConfigurableListableBeanFactory clbf = ((AbstractApplicationContext) ctx).getBeanFactory();
		for (String name : all) {
			Object s = clbf.getSingleton(name);
			if (s != null)
				singletons.add(s);
		}

		return singletons;
	}
	
	public static <T> boolean hasInstanceOfClass(ApplicationContext context, Class<T> beanClass) {
		
		List<Object> beans = BeanUtils.getInstantiatedSigletons(context);
		for (Object b : beans) {
			if (b.getClass().isAssignableFrom(beanClass)) {
				return true;
			}
		}
		return false;
	}
}
