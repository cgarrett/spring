package org.sample;

import org.sample.plugin.ApplicationPluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

	private ApplicationPluginManager pluginManager = null;
	private static Logger log = LoggerFactory.getLogger(Application.class);
	
	public void setPluginManager(ApplicationPluginManager pm) {
		this.pluginManager = pm;
	}
	
	public String getName() { return "Application"; }

}
