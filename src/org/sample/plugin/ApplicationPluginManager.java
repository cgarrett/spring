package org.sample.plugin;

import java.util.Map;

import org.sample.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationPluginManager implements ApplicationContextAware, DisposableBean {

	private ApplicationContext appContext = null;
	private static Logger log = LoggerFactory.getLogger(ApplicationPluginManager.class);
	private Application app = null;
	
	ApplicationPluginManager(Application a) {
		log.info("Ctor()");
		this.app = a;
	}
	
	private void register() {
		Map<String,ApplicationPlugin> plugins = this.appContext.getBeansOfType(ApplicationPlugin.class, true, false);
		for (String pluginName: plugins.keySet()) {
			log.info(">>> register plugin: " + pluginName);
			ApplicationPlugin p = plugins.get(pluginName);
			p.register(this.app);
		}
	}

	private void unregister() {
		Map<String,ApplicationPlugin> plugins = this.appContext.getBeansOfType(ApplicationPlugin.class, true, false);
		for (String pluginName: plugins.keySet()) {
			log.info(">>> unregister plugin: " + pluginName);
			ApplicationPlugin p = plugins.get(pluginName);
			p.unregister();
		}
	}	
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		log.info("setAppContext()");
		this.appContext = arg0;

		register();
	}

	@Override
	public void destroy() throws Exception {
		unregister();
	}
}
