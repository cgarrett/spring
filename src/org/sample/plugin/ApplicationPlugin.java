package org.sample.plugin;

import org.sample.Application;
import org.springframework.context.annotation.Scope;

@Scope("prototype")
public interface ApplicationPlugin {
	void register(Application app);
	void unregister();
}
